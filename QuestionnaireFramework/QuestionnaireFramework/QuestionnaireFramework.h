#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_QuestionnaireFramework.h"
#include "MainMenu.h"
#include "Database.h"
#include "Student.h"
#include "RegisterFrame.h"
#include "AddQuestion.h"

class QuestionnaireFramework : public QMainWindow
{
	Q_OBJECT

public:
	QuestionnaireFramework(QWidget* parent = Q_NULLPTR);

protected:
	void showEvent(QShowEvent* ev);

private slots:
	void on_loginButton_pressed();
	bool check_capatcha(const QString& newString);
	void on_registerButton_pressed();

private:
	Ui::QuestionnaireFrameworkClass ui;
	QString m_username;
	QString m_password;
	Database database;
	Student m_student;
	MainMenu m_mainMenu;

private:
	void setUsername(const QString& username);
	void setPassword(const QString& password);
	void verifyStudentAccount();
	void verifyTeacherAccount();
	void playSound();
	bool checkPasswordHash(const std::string&, const std::string&);

	QString getUsername()const;
	QString getPassword()const;
	RegisterFrame regFrame;
	AddQuestion m_AddQuestion;
	QString m_captcha;
};
