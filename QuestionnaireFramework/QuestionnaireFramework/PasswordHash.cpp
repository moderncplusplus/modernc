#include "PasswordHash.h"
#include <CkCrypt2.h>
#include <exception>
#include "../Logging/LoggerSingleton.h"

bool PasswordHash::hashPassword(const char* password, const char*& hashedPassword)
{
	try
	{
		const char* bcryptHash = cryptObj.bCryptHash(password);
		hashedPassword = bcryptHash;

		if (hashedPassword != NULL)
		{
			return true;
		}
		else
		{
			return false;
		}

	}
	catch (std::exception& exception)
	{
		LoggerSingleton::getLoggerInstance().log("Hashing error" + std::string(exception.what()) + "\n", Logger::Level::Error);
	}

	return false;
}

bool PasswordHash::verifyPassword(const char* password, const char* cryptHash)
{
	try
	{

		bool passwordValid = cryptObj.BCryptVerify(password, cryptHash);

		if (passwordValid == true)
		{
			return true;
		}
		else
		{
			return false;
		}

	}
	catch (std::exception& exception)
	{
		LoggerSingleton::getLoggerInstance().log("Error during password verification" + std::string(exception.what()) + "\n", Logger::Level::Error);
	}

	return false;
}

void PasswordHash::setWorkFactor(const int& workFactor)
{
	try
	{

		if (workFactor < 4 || workFactor > 31)
		{
			throw "Invalid workFactor! Please enter a valid work factor between 4 and 31.\n";
		}
		else
		{
			cryptObj.put_BCryptWorkFactor(workFactor);
		}
	}
	catch (std::exception& exception) {

		LoggerSingleton::getLoggerInstance().log("Error:" + std::string(exception.what()) + "\n", Logger::Level::Error);
	}

}

void PasswordHash::testMethod()
{

	setWorkFactor(10);
	const char* bcryptHash = "n", * pass = "mihai23";

	hashPassword(pass, bcryptHash);
	LoggerSingleton::getLoggerInstance().log("pass " + std::string(pass) + "  bcryptHash:" + std::string(bcryptHash) + "\n", Logger::Level::Info);

	const char* copy = bcryptHash;

	if (verifyPassword(pass, copy))
	{
		LoggerSingleton::getLoggerInstance().log("Status: verificat\n", Logger::Level::Info);
	}
	else
	{
		LoggerSingleton::getLoggerInstance().log("Status: nu merge\n", Logger::Level::Warning);
	}

}
