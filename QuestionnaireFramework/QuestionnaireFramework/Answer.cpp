#include "Answer.h"
#include "../Logging/LoggerSingleton.h"

Answer::Answer() {}

Answer::Answer(const int id, const int questionId) :
	m_id(id), m_questionId(questionId)
{
}

Answer::Answer(const int id, const int questionId, const std::string& correctAnswer) :
	m_id(id), m_questionId(questionId), m_correctAnswer(correctAnswer)
{
}

Answer::~Answer() {}

std::string Answer::getAnswer(const int option) const
{
	switch (option) {

	case 1:
		return m_answerA;
		break;

	case 2:
		return m_answerB;
		break;

	case 3:
		return m_answerC;
		break;

	case 4:
		return m_answerD;
		break;

	default:
		LoggerSingleton::getLoggerInstance().log("Invalid argument\n", Logger::Level::Warning);
	}

}

void Answer::setAnswer(const std::string& newAnswer, int option)
{
	switch (option)
	{

	case 1:
		this->m_answerA = newAnswer;
		break;
	case 2:
		this->m_answerB = newAnswer;
		break;
	case 3:
		this->m_answerC = newAnswer;
		break;
	case 4:
		this->m_answerD = newAnswer;
		break;

	default:
		LoggerSingleton::getLoggerInstance().log("Invalid argument\n", Logger::Level::Warning);
		break;
	}
}

void Answer::setAllAnswers(const std::string& answerA, const std::string& answerB, const std::string& answerC, const std::string& answerD)
{
	m_answerA = answerA;
	m_answerB = answerB;
	m_answerC = answerC;
	m_answerD = answerD;

}

std::string Answer::getCorrectAnswer() const
{
	return m_correctAnswer;
}

void Answer::setStudentAnswer(const std::string& newCorrectAnswer)
{
	m_correctAnswer = newCorrectAnswer;
}

int Answer::getId() const
{
	return m_id;
}

void Answer::setId(const int newId)
{
	m_id = newId;
}

int Answer::getQuestionId() const
{
	return m_questionId;
}

void Answer::setQuestionId(const int newId)
{
	m_questionId = newId;
}

bool Answer::operator==(const Answer& otherAnswer) const
{
	return (this->m_id == otherAnswer.m_id && this->m_questionId == otherAnswer.m_questionId);
}

std::ostream& operator<<(std::ostream& output, const Answer& answer)
{
	output << answer.m_id << "\n" << answer.m_answerA << " \n" << answer.m_answerB << "\n" << answer.m_answerC << "\n" << answer.m_answerD << "\n";
	return output;
}
