#include "Question.h"

Question::Question()
{
}

Question::Question(const std::string& question_text, int question_id, int chapter) :
	m_question_text(question_text), m_question_id(question_id), m_chapter(chapter)
{

}


Question::~Question()
{
}


std::string Question::getQuestion() const
{
	return m_question_text;
}

void Question::setQuestion(const std::string& question_text, int question_id, int chapter)
{
	m_question_id = question_id;
	m_question_text = question_text;
	m_chapter = chapter;
}

Answer Question::getCorrectAnswer()	const
{
	return m_correctAnswer;
}

void Question::setCorrectAnswer(const Answer& answer)
{
	this->m_correctAnswer = answer;
}

std::vector<Answer> Question::getAllAnswers() const
{
	return m_possibleAnswers;
}

void Question::setPossibleAnswers(const std::vector<Answer>& newPossibleAnswers)
{
	this->m_possibleAnswers = newPossibleAnswers;
}

bool Question::checkAnswer(const Answer& userAnswer) const
{
	return (this->m_correctAnswer == userAnswer);
}

float Question::questionScoreCalc(const Answer& userAnswer) const
{
	int maxScore = 1, minScore = 0;

	if (checkAnswer(userAnswer))
		return maxScore;

	return minScore;
}

int Question::getQuestionId() const
{
	return m_question_id;
}

int Question::getChapter() const
{
	return m_chapter;
}

std::ostream& operator<<(std::ostream& output, const Question& question)
{
	output << question.m_question_id << " " << question.m_question_text << " " << question.m_chapter;
	return output;
}
