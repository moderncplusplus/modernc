#include "MainMenu.h"
#include <QtWidgets/qmessagebox.h>
#include <fstream>
#include <sstream>
#include <string>

#define SQL_RESULT_LEN 50

MainMenu::MainMenu(QWidget* parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	ui.stackedWidget->setCurrentIndex(0);
	ui.Time->setText(QDateTime::currentDateTime().toString());

	QTimer* timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(updateTime()));
	timer->start(1000);
}

MainMenu::~MainMenu()
{
}

void MainMenu::setStudent(Student&& student)
{
	m_student = std::move(student);
}

void MainMenu::setGrades()
{
	SQLHANDLE sqlStmtHandle = m_student.getStmtHandle();
	m_questionnaire.setSqlStmtHandle(sqlStmtHandle);

	SQLFreeStmt(sqlStmtHandle, SQL_CLOSE);

	std::string id = std::to_string(m_student.getId());
	std::string query_string = "SELECT ISNULL(BD, -1) AS BD, ISNULL(FP, -1) AS FP, ISNULL(POO, -1) AS POO, ISNULL(SD, -1) AS SD FROM Grades WHERE student_id =" + id;
	std::wstring  query_wstring(query_string.begin(), query_string.end());
	SQLWCHAR* SQLQuery = (SQLWCHAR*)query_wstring.c_str();

	LoggerSingleton::getLoggerInstance().log("Executing T-SQL Grades setting query...\n", Logger::Level::Info);
	//if there is a problem executing the query then exit application else display query result
	if (SQL_SUCCESS != SQLExecDirect(sqlStmtHandle, SQLQuery, SQL_NTS)) {
		LoggerSingleton::getLoggerInstance().log("Error querying SQL Server\n", Logger::Level::Error);
	}

	else {

		int BD;
		int FP;
		int POO;
		int SD;

		while (SQLFetch(sqlStmtHandle) == SQL_SUCCESS) {
			// Fetches the next rowset of data from the result
			SQLGetData(sqlStmtHandle, 1, SQL_C_DEFAULT, &BD, sizeof(BD), NULL);
			SQLGetData(sqlStmtHandle, 2, SQL_C_DEFAULT, &FP, sizeof(FP), NULL);
			SQLGetData(sqlStmtHandle, 3, SQL_C_DEFAULT, &POO, sizeof(POO), NULL);
			SQLGetData(sqlStmtHandle, 4, SQL_C_DEFAULT, &SD, sizeof(SD), NULL);

			//Fetching the current grades from the database and inserting them 
			m_student.insertGrade("BD", BD);
			m_student.insertGrade("FP", FP);
			m_student.insertGrade("POO", POO);
			m_student.insertGrade("SD", SD);

		}
		SQLFreeStmt(sqlStmtHandle, SQL_CLOSE);
	}
}

void MainMenu::showEvent(QShowEvent* ev)
{
	QWidget::showEvent(ev);
	//Retrieve additional data of the student from the database
	SQLHANDLE sqlStmtHandle = m_student.getStmtHandle();
	SQLFreeStmt(sqlStmtHandle, SQL_CLOSE);

	std::string id = std::to_string(m_student.getId());
	std::string query_string = "SELECT * FROM Student_details WHERE id=" + id;
	std::wstring  query_wstring(query_string.begin(), query_string.end());
	SQLWCHAR* SQLQuery = (SQLWCHAR*)query_wstring.c_str();

	LoggerSingleton::getLoggerInstance().log("Executing T-SQL Student details query...\n", Logger::Level::Info);
	//if there is a problem executing the query then exit application else display query result
	if (SQL_SUCCESS != SQLExecDirect(sqlStmtHandle, SQLQuery, SQL_NTS)) {
		LoggerSingleton::getLoggerInstance().log("Error querying SQL Server\n", Logger::Level::Error);
	}

	else {

		char  email[SQL_RESULT_LEN];
		char  grupa[SQL_RESULT_LEN];
		char  specializare[SQL_RESULT_LEN];
		char  facultate[SQL_RESULT_LEN];

		while (SQLFetch(sqlStmtHandle) == SQL_SUCCESS) {
			// Fetches the next rowset of data from the result
			SQLGetData(sqlStmtHandle, 2, SQL_C_DEFAULT, &email, sizeof(email), NULL);
			SQLGetData(sqlStmtHandle, 3, SQL_C_DEFAULT, &grupa, sizeof(grupa), NULL);
			SQLGetData(sqlStmtHandle, 4, SQL_C_DEFAULT, &specializare, sizeof(specializare), NULL);
			SQLGetData(sqlStmtHandle, 5, SQL_C_DEFAULT, &facultate, sizeof(facultate), NULL);

			m_student.setEmail(email);
			m_student.setGroup(grupa);
			m_student.setSpecialization(specializare);
			m_student.setFaculty(facultate);

			ui.email->setText(m_student.getEmail());
			ui.grupa->setText(m_student.getGroup());
			ui.specializare->setText(m_student.getSpecialization());
			ui.facultate->setText(m_student.getFaculty());

		}
		SQLFreeStmt(sqlStmtHandle, SQL_CLOSE);
	}

	m_bd_frame.setStudent(m_student);
	m_sd_frame.setStudent(m_student);
	m_poo_frame.setStudent(m_student);
	m_fp_frame.setStudent(m_student);

	setGrades();
}

void MainMenu::on_FPButton_pressed()
{
	if (m_isMuted)
	{
		m_fp_frame.setIsMuteTrue();
		m_fp_frame.setSoundOff();
	}
	else
	{
		m_fp_frame.setIsMuteFalse();
		m_fp_frame.setSoundOn();
	}
	if (m_student.retrieveGrade("FP") == -1)
	{
		m_questionnaire.setSubject("FP");

		m_questionnaire.chooseQuestions();
		m_questionnaire.writeQuestions();

		m_questionnaire.chooseAnswers();
		m_questionnaire.writeAnswers();

		m_fp_frame.addQuestionsToFrame(m_questionnaire.getQuestions());
		m_fp_frame.addAnswersToFrame(m_questionnaire.getAnswer());

		m_fp_frame.setCourseSubject("FP");
		m_fp_frame.setBGColor(m_bgColor);
		m_fp_frame.show();

		m_student.setGrade("FP", m_fp_frame.getGrade());

	}
	else
		QMessageBox::warning(this, "Numarul maxim de incercari a fost atins!", "Nu mai puteti da niciun test la aceasta materie!");
	if (!m_isMuted)
		playSound();
}



void MainMenu::on_BDButton_pressed()
{
	if (m_isMuted)
	{
		m_bd_frame.setIsMuteTrue();
		m_bd_frame.setSoundOff();
	}
	else
	{
		m_bd_frame.setIsMuteFalse();
		m_bd_frame.setSoundOn();
	}
	if (m_student.retrieveGrade("BD") == -1)
	{
		m_questionnaire.setSubject("BD");

		m_questionnaire.chooseQuestions();
		m_questionnaire.writeQuestions();

		m_questionnaire.chooseAnswers();
		m_questionnaire.writeAnswers();

		m_bd_frame.addQuestionsToFrame(m_questionnaire.getQuestions());
		m_bd_frame.addAnswersToFrame(m_questionnaire.getAnswer());

		m_bd_frame.setBGColor(m_bgColor);

		m_bd_frame.setCourseSubject("BD");
		m_bd_frame.show();

		m_student.setGrade("BD", m_bd_frame.getGrade());
	}
	else
		QMessageBox::warning(this, "Numarul maxim de incercari a fost atins!", "Nu mai puteti da niciun test la aceasta materie!");
	if (!m_isMuted)
		playSound();
}

void MainMenu::on_SDButton_pressed()
{
	if (m_isMuted)
	{
		m_sd_frame.setIsMuteTrue();
		m_sd_frame.setSoundOff();
	}
	else
	{
		m_sd_frame.setIsMuteFalse();
		m_sd_frame.setSoundOn();
	}
	if (m_student.retrieveGrade("SD") == -1)
	{
		m_questionnaire.setSubject("SD");

		m_questionnaire.chooseQuestions();
		m_questionnaire.writeQuestions();

		m_questionnaire.chooseAnswers();
		m_questionnaire.writeAnswers();

		m_sd_frame.addQuestionsToFrame(m_questionnaire.getQuestions());
		m_sd_frame.addAnswersToFrame(m_questionnaire.getAnswer());

		m_sd_frame.setBGColor(m_bgColor);

		m_sd_frame.setCourseSubject("SD");
		m_sd_frame.show();

		m_student.setGrade("SD", m_sd_frame.getGrade());
	}
	else
		QMessageBox::warning(this, "Numarul maxim de incercari a fost atins!", "Nu mai puteti da niciun test la aceasta materie!");
	if (!m_isMuted)
		playSound();
}

void MainMenu::on_POOButton_pressed()
{
	if (m_isMuted)
	{
		m_poo_frame.setIsMuteTrue();
		m_poo_frame.setSoundOff();
	}
	else
	{
		m_poo_frame.setIsMuteFalse();
		m_poo_frame.setSoundOn();
	}
	if (m_student.retrieveGrade("POO") == -1)
	{
		m_questionnaire.setSubject("POO");

		m_questionnaire.chooseQuestions();
		m_questionnaire.writeQuestions();

		m_questionnaire.chooseAnswers();
		m_questionnaire.writeAnswers();

		m_poo_frame.addQuestionsToFrame(m_questionnaire.getQuestions());
		m_poo_frame.addAnswersToFrame(m_questionnaire.getAnswer());

		m_poo_frame.setBGColor(m_bgColor);

		m_poo_frame.setCourseSubject("POO");
		m_poo_frame.show();

		m_student.setGrade("POO", m_poo_frame.getGrade());
	}
	else
		QMessageBox::warning(this, "Numarul maxim de incercari a fost atins!", "Nu mai puteti da niciun test la aceasta materie!");
	if (!m_isMuted)
		playSound();
}

void MainMenu::on_Settings_pressed()
{
	ui.stackedWidget->setCurrentIndex(1);
	if (!m_isMuted)
		playSound();
}

void MainMenu::on_backButton_pressed()
{
	ui.stackedWidget->setCurrentIndex(0);
	if (!m_isMuted)
		playSound();
}

void MainMenu::updateMuteLabel()
{
	if (m_isMuted)
	{
		ui.sound_text->setText("   OPRIT");
	}
	else
	{
		ui.sound_text->setText("   PORNIT");
	}
}

void MainMenu::on_sound_button_pressed()
{

	if (m_isMuted)
	{
		m_isMuted = false;
	}
	else
	{
		m_isMuted = true;
	}
	updateMuteLabel();
	if (!m_isMuted)
		playSound();
}

void MainMenu::on_changeBackgroundButton_pressed()
{
	QColor color = QColorDialog::getColor(Qt::white, this, "Alege culoarea");
	m_bgColor = "background-color:" + color.name();
	if (!m_isMuted)
		playSound();
}

void MainMenu::on_backColorButton_pressed()
{
	if (!m_isMuted)
		playSound();
	ui.stackedWidget->setCurrentIndex(1);
}

void MainMenu::on_changeColor_pressed()
{
	if (!m_isMuted)
		playSound();
	changeColor();
}

void MainMenu::updateTime()
{
	ui.Time->setText(QDateTime::currentDateTime().toString());
}

void MainMenu::playSound()
{
	QMediaPlayer* soundPlayer = new QMediaPlayer();
	soundPlayer->setMedia(QUrl("qrc:/new/sounds/button_pressed.mp3"));
	soundPlayer->setVolume(50);
	soundPlayer->play();
}

void MainMenu::changeColor()
{
	ui.page->setStyleSheet(m_bgColor);
	ui.page_2->setStyleSheet(m_bgColor);
	ui.frame->setStyleSheet(m_bgColor);
	this->setStyleSheet(m_bgColor);
}
