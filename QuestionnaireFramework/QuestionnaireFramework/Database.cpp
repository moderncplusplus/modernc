#include "Database.h"
#include "Networking.h"

#define SQL_RETURN_CODE_LEN 1000

bool CheckNetworkConnection()
{
	LoggerSingleton::getLoggerInstance().log("Veryfing network connection...\n", Logger::Level::Info);
	Networking networking;
	return (networking.networkConnection_QuickMode() || networking.networkConnection_AdvancedMode());
}

Database::Database()
{
	m_isConnected = false;
	m_sqlConnHandle = NULL;
	m_sqlStmtHandle = NULL;
	m_sqlEnvHandle = NULL;

	if (CheckNetworkConnection())
	{
		LoggerSingleton::getLoggerInstance().log("You have a internet connection!\n", Logger::Level::Info);
		connectToDatabase();
	}
	else
	{
		LoggerSingleton::getLoggerInstance().log("Failed to connect to the internet! Please check yout internet connection!\n", Logger::Level::Warning);
	}
}

void Database::connectToDatabase()
{
	SQLWCHAR retconstring[SQL_RETURN_CODE_LEN];

	//allocations
	if (SQL_SUCCESS != SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &m_sqlEnvHandle))
	{
		disconnectDatabase();
		return;
	}
	if (SQL_SUCCESS != SQLSetEnvAttr(m_sqlEnvHandle, SQL_ATTR_ODBC_VERSION, (SQLPOINTER)SQL_OV_ODBC3, 0))
	{
		disconnectDatabase();
		return;
	}
	if (SQL_SUCCESS != SQLAllocHandle(SQL_HANDLE_DBC, m_sqlEnvHandle, &m_sqlConnHandle))
	{
		disconnectDatabase();
		return;
	}

	LoggerSingleton::getLoggerInstance().log("Attempting connection to SQL Server...\n", Logger::Level::Info);

	//connect to SQL Server  
	switch (SQLDriverConnect(m_sqlConnHandle,
		NULL,
		(SQLWCHAR*)L"DRIVER={SQL Server};SERVER=109.99.119.137, 1433;DATABASE=Test;UID=Elev;PWD=Elev;Trusted=true;",
		SQL_NTS,
		retconstring,
		1024,
		NULL,
		SQL_DRIVER_NOPROMPT)) {
	case SQL_SUCCESS:
		m_isConnected = true;
		LoggerSingleton::getLoggerInstance().log("Successfully connected to SQL Server\n", Logger::Level::Info);
		break;
	case SQL_SUCCESS_WITH_INFO:
		m_isConnected = true;
		LoggerSingleton::getLoggerInstance().log("Successfully connected to SQL Server\n", Logger::Level::Info);
		break;
	case SQL_INVALID_HANDLE:
		LoggerSingleton::getLoggerInstance().log("Could not connect to SQL Server\n", Logger::Level::Error);
		disconnectDatabase();
		break;
	case SQL_ERROR:
		LoggerSingleton::getLoggerInstance().log("Could not connect to SQL Server\n", Logger::Level::Error);
		disconnectDatabase();
		break;
	default:
		break;
	}

	//if there is a problem connecting then exit application
	if (SQL_SUCCESS != SQLAllocHandle(SQL_HANDLE_STMT, m_sqlConnHandle, &m_sqlStmtHandle))
	{
		disconnectDatabase();
		return;
	}
}

void Database::disconnectDatabase()
{
	m_isConnected = false;
	SQLFreeHandle(SQL_HANDLE_STMT, m_sqlStmtHandle);
	SQLDisconnect(m_sqlConnHandle);
	SQLFreeHandle(SQL_HANDLE_DBC, m_sqlConnHandle);
	SQLFreeHandle(SQL_HANDLE_ENV, m_sqlEnvHandle);
	LoggerSingleton::getLoggerInstance().log("Disconnected from database!\n", Logger::Level::Info);

}

Database::~Database()
{
	disconnectDatabase();
}

SQLHANDLE Database::getConnHandle() const
{
	return m_sqlConnHandle;
}

SQLHANDLE Database::getStmtHandle() const
{
	return m_sqlStmtHandle;
}

SQLHANDLE Database::getEnvHandle() const
{
	return m_sqlEnvHandle;
}

bool Database::getIsConnected() const
{
	return m_isConnected;
}

void Database::setStmtHandle(const SQLHANDLE& handle)
{
	m_sqlStmtHandle = handle;
}
