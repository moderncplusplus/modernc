#pragma once

#include <QWidget>
#include <QTime>
#include <QTimer>
#include <QString>
#include <qmessagebox.h>
#include "ui_CourseFrame.h"
#include "Student.h"
#include "Question.h"
#include "Answer.h"
#include "Email.h"
#include <unordered_map>
#include <tuple>

class CourseFrame : public QWidget
{
	Q_OBJECT

public:
	CourseFrame(QWidget* parent = Q_NULLPTR);
	~CourseFrame();
	QTime time;
	QTimer* timer;

	void addQuestionsToFrame(const std::vector<Question>&);
	void addAnswersToFrame(const std::vector<Answer>&);

private:

	enum class Sound {

		BUTTON_PRESSED,
		LOGIN,
		FINAL_TEST,
		COUNTDOWN,
		CHECKBOX
	};

	Ui::CourseFrame ui;
	bool startButtonpressed = false;

	std::vector<Question> m_questions;
	std::vector<Question> m_QuizQuestions;
	std::vector<Answer> m_answers;
	std::map<int, std::string> m_answersByStudent;
	std::vector<std::tuple<bool, bool, bool, bool, bool>> m_checkboxState;

	Student m_student;
	std::string m_courseSubject;

	int m_currentQuestion;
	double m_finalGrade = 0;
	bool m_isMuted = false;


	std::string m_startTime;
	std::string m_endTime;
	Email m_email;

	const int MAXQUESTIONS = 20;

private:
	void setAnswerText(const int);
	void setQuestionText(const int);
	int generateRandomNumber();
	std::string checkAnswer();
	void clearCheckboxes();

	void modifyGrade();
	void sendEmail();

	void initCheckboxState();
	void emailTextStudentAnswers() const;
	void updateCheckboxState();
	void checkNextButton();
	void checkPreviousButton();

	std::string getSound(const Sound&);
	void updateMuteLabel();
	void shuffleQuestionsAnswers();

private slots:
	void playSound();
	void updateCountdown();

	void on_start_button_pressed();
	void on_finish_button_pressed();
	void on_main_menu_button_pressed();
	void on_next_button_pressed();
	void on_previous_button_pressed();
	void on_sound_button_pressed();


public:
	void setStudent(const Student&);
	void setCourseSubject(const std::string&);
	void setIsMuteFalse();
	void setIsMuteTrue();
	void setSoundOff();
	void setSoundOn();
	void setBGColor(QString color);
	int getGrade();

};
