#include "Student.h"



Student::Student()
{
	m_email = "";
	m_faculty = "";
	m_group = "";
	m_id = 0;
	m_specialization = "";
	m_sqlStmtHandle = NULL;
}

Student::Student(Student&& student)
{
	*this = std::move(student);
}

Student::Student(const Student& student)
{
	m_sqlStmtHandle = student.m_sqlStmtHandle;
	m_email = student.m_email;
	m_faculty = student.m_faculty;
	m_group = student.m_group;
	m_id = student.m_id;
	m_specialization = student.m_specialization;
}

Student::~Student()
{

}

Student& Student::operator=(const Student& student)
{
	m_sqlStmtHandle = student.m_sqlStmtHandle;
	m_email = student.m_email;
	m_faculty = student.m_faculty;
	m_group = student.m_group;
	m_id = student.m_id;
	m_specialization = student.m_specialization;
	m_grades = student.m_grades;

	return *this;
}

Student& Student::operator=(Student&& student)
{
	this->m_sqlStmtHandle = student.m_sqlStmtHandle;
	this->m_email = student.m_email;
	this->m_faculty = student.m_faculty;
	this->m_group = student.m_group;
	this->m_id = student.m_id;
	this->m_specialization = student.m_specialization;
	this->m_grades = student.m_grades;

	new (&student)Student;
	return *this;
}



void Student::setHandle(const SQLHANDLE& sqlStmtHandle)
{
	m_sqlStmtHandle = sqlStmtHandle;
}

void Student::setId(int id)
{
	m_id = id;
}

void Student::setEmail(const QString& email)
{
	m_email = email;
}

void Student::setGroup(const QString& group)
{
	m_group = group;
}

void Student::setSpecialization(const QString& specialization)
{
	m_specialization = specialization;
}

void Student::setFaculty(const QString& faculty)
{
	m_faculty = faculty;
}

int Student::getId() const
{
	return m_id;
}

QString Student::getEmail() const
{
	return m_email;
}

QString Student::getGroup() const
{
	return m_group;
}

QString Student::getSpecialization() const
{
	return m_specialization;
}

QString Student::getFaculty() const
{
	return m_faculty;
}

SQLHANDLE Student::getStmtHandle() const
{
	return m_sqlStmtHandle;
}

void Student::insertGrade(const std::string& course_name, int grade)
{
	std::pair<std::string, int>pair(course_name, grade);
	m_grades.insert(pair);
}

int Student::retrieveGrade(const std::string& course_name) const
{
	return m_grades.at(course_name);
}

void Student::setGrade(const std::string& course_name, int grade)
{
	m_grades[course_name] = grade;
}

void Student::insertGradeToDatabase(const std::string& course_name, int grade)
{
	LoggerSingleton::getLoggerInstance().log("Executing T-SQL Grade update query...\n", Logger::Level::Info);
	SQLFreeStmt(m_sqlStmtHandle, SQL_CLOSE);

	std::string query_string = "UPDATE Grades SET " + course_name + "= ? WHERE student_id = ?";
	std::wstring  query_wstring(query_string.begin(), query_string.end());
	SQLWCHAR* SQLQuery = (SQLWCHAR*)query_wstring.c_str();

	SQLRETURN retcode = SQLPrepare(m_sqlStmtHandle, SQLQuery, SQL_NTS);
	retcode = SQLBindParameter(m_sqlStmtHandle, 1, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &grade, 0, NULL);
	retcode = SQLBindParameter(m_sqlStmtHandle, 2, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &m_id, 0, NULL);

	retcode = SQLExecute(m_sqlStmtHandle);

	if (SQL_SUCCESS != retcode) {
		LoggerSingleton::getLoggerInstance().log("Error querying SQL Server\n", Logger::Level::Error);

	}

	SQLFreeStmt(m_sqlStmtHandle, SQL_CLOSE);

}
