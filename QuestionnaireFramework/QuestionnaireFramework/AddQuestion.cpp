#include "AddQuestion.h"
#include <QtWidgets/qmessagebox.h>
#include <algorithm>

#define MAX_ANSWERS 4

AddQuestion::AddQuestion(QWidget* parent)
	: QWidget(parent)
{
	ui.setupUi(this);
}

AddQuestion::~AddQuestion()
{
	
}

std::string AddQuestion::getCourse()const
{
	int course_id = ui.comboBox_course->currentIndex();
	switch (course_id)
	{
	case 0: return "BD";
	case 1:return "POO";
	case 2: return "SD";
	case 3: return "FP";
	default: return "";
	}
}

std::string AddQuestion::getAnswer()const
{
	std::string answer;

	if (ui.checkBox_1->checkState() == Qt::Checked)
		answer += "a";
	if (ui.checkBox_2->checkState() == Qt::Checked)
		answer += "b";
	if (ui.checkBox_3->checkState() == Qt::Checked)
		answer += "c";
	if (ui.checkBox_4->checkState() == Qt::Checked)
		answer += "d";

	return answer;

}

bool AddQuestion::verifyQuestion(std::string& question_text, std::vector<std::string>& answers, std::string& correct_answer) const
{
	std::sort(answers.begin(), answers.end());
	auto iterator = std::unique(answers.begin(), answers.end());
	answers.resize(std::distance(answers.begin(), iterator));

	if (answers.size() != MAX_ANSWERS)
		return false;
	
	for (int index = 0; index < answers.size(); index++)
		if (answers[index].size() == 0)
			return false;

	if (correct_answer.size() == 0)
		return false;

	if (question_text.size() == 0)
		return false;

	return true;
}

void AddQuestion::insertQuestion()
{
	LoggerSingleton::getLoggerInstance().log("Executing T-SQL Question insert query...\n", Logger::Level::Info);

	SQLRETURN retcode = SQLPrepare(m_sqlStmtHandle, (SQLWCHAR*)L"INSERT INTO Questions (subject, question_text, chapter) OUTPUT Inserted.question_id VALUES(?,?,?) ", SQL_NTS);
	retcode = SQLBindParameter(m_sqlStmtHandle, 1, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 50, 0, (SQLPOINTER)m_course.c_str(), m_course.length(), NULL);
	retcode = SQLBindParameter(m_sqlStmtHandle, 2, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 500, 0, (SQLPOINTER)m_question_text.c_str(), m_question_text.length(), NULL);
	retcode = SQLBindParameter(m_sqlStmtHandle, 3, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &m_chapter, 0, NULL);
	retcode = SQLExecute(m_sqlStmtHandle);

	if (SQL_SUCCESS != retcode) {
		LoggerSingleton::getLoggerInstance().log("Error querying SQL Server\n", Logger::Level::Error);

	}
	else {
		while (SQLFetch(m_sqlStmtHandle) == SQL_SUCCESS) {

			SQLGetData(m_sqlStmtHandle, 1, SQL_C_DEFAULT, &m_question_id, sizeof(m_question_id), NULL);
		}

		SQLFreeStmt(m_sqlStmtHandle, SQL_CLOSE);
	}
}

void AddQuestion::insertAnswer()
{
	LoggerSingleton::getLoggerInstance().log("Executing T-SQL Answer insert query...\n", Logger::Level::Info);

	SQLRETURN retcode = SQLPrepare(m_sqlStmtHandle, (SQLWCHAR*)L"INSERT INTO Answer (question_id, answer_a, answer_b,answer_c,answer_d, student_answer) VALUES(?,?,?,?,?,?) ", SQL_NTS);
	retcode = SQLBindParameter(m_sqlStmtHandle, 1, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &m_question_id, 0, NULL);
	retcode = SQLBindParameter(m_sqlStmtHandle, 2, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 500, 0, (SQLPOINTER)std::get<0>(m_answers).c_str(), std::get<0>(m_answers).length(), NULL);
	retcode = SQLBindParameter(m_sqlStmtHandle, 3, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 500, 0, (SQLPOINTER)std::get<1>(m_answers).c_str(), std::get<1>(m_answers).length(), NULL);
	retcode = SQLBindParameter(m_sqlStmtHandle, 4, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 500, 0, (SQLPOINTER)std::get<2>(m_answers).c_str(), std::get<2>(m_answers).length(), NULL);
	retcode = SQLBindParameter(m_sqlStmtHandle, 5, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 500, 0, (SQLPOINTER)std::get<3>(m_answers).c_str(), std::get<3>(m_answers).length(), NULL);
	retcode = SQLBindParameter(m_sqlStmtHandle, 6, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 5, 0, (SQLPOINTER)m_answer.c_str(), m_answer.length(), NULL);

	retcode = SQLExecute(m_sqlStmtHandle);
	if (SQL_SUCCESS != retcode) {
		LoggerSingleton::getLoggerInstance().log("Error querying SQL Server\n", Logger::Level::Error);

	}

	SQLFreeStmt(m_sqlStmtHandle, SQL_CLOSE);
}

void AddQuestion::printQuestionAndAnswers() const
{
	LoggerSingleton::getLoggerInstance().log("Intrebare noua: " + m_question_text + "\n"
		+ "Varianta A: " + std::get<0>(m_answers) + "\n"
		+ "Varianta B: " + std::get<1>(m_answers) + "\n"
		+ "Varianta C: " + std::get<2>(m_answers) + "\n"
		+ "Varianta D: " + std::get<3>(m_answers) + "\n"
		+ "Capitol: " + std::to_string(m_chapter) + "\n"
		+ "Materie: " + m_course + "\n"
		+ "Raspuns: " + m_answer + "\n", Logger::Level::Info);
}

SQLHANDLE AddQuestion::getStmtHandle() const
{
	return m_sqlStmtHandle;
}

void AddQuestion::setStmtHandle(const SQLHANDLE& handle)
{
	m_sqlStmtHandle = handle;
}

void AddQuestion::on_pushButton_pressed()
{
	std::vector<std::string>answers;

	m_question_text = ui.question_text->toPlainText().toUtf8().constData();
	std::get<0>(m_answers) = ui.answer_1->toPlainText().toUtf8().constData();
	std::get<1>(m_answers) = ui.answer_2->toPlainText().toUtf8().constData();
	std::get<2>(m_answers) = ui.answer_3->toPlainText().toUtf8().constData();
	std::get<3>(m_answers) = ui.answer_4->toPlainText().toUtf8().constData();

	answers.emplace_back(std::get<0>(m_answers));
	answers.emplace_back(std::get<1>(m_answers));
	answers.emplace_back(std::get<2>(m_answers));
	answers.emplace_back(std::get<3>(m_answers));

	m_chapter = ui.comboBox_chapter->currentIndex() + 1;
	m_course = getCourse();
	m_answer = getAnswer();

	if (verifyQuestion(m_question_text, answers, m_answer))
	{
		m_sqlStmtHandle = getStmtHandle();

		printQuestionAndAnswers();

		insertQuestion();
		insertAnswer();

	}
	else
		QMessageBox::warning(this, "Question", "The question has errros.");
}