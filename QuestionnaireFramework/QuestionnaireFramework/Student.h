#pragma once
#include <QString>
#include "Database.h"
#include <unordered_map>
class Student
{
public:

	Student();
	Student(Student&& student);
	Student(const Student& student);
	~Student();

	Student& operator=(const Student& student);
	Student& operator=(Student&& student);


	void setHandle(const SQLHANDLE& sqlStmtHandle);
	void setId(int id);
	void setEmail(const QString& email);
	void setGroup(const QString& group);
	void setSpecialization(const QString& specialization);
	void setFaculty(const QString& faculty);

	int getId()const;
	QString getEmail()const;
	QString getGroup()const;
	QString getSpecialization()const;
	QString getFaculty()const;
	SQLHANDLE getStmtHandle()const;

	void insertGrade(const std::string& course_name, int grade);
	int retrieveGrade(const std::string& course_name)const;
	void setGrade(const std::string& course_name, int grade);
	void insertGradeToDatabase(const std::string& course_name, int grade);

private:
	int m_id;
	QString m_email;
	QString m_group;
	QString m_specialization;
	QString m_faculty;
	SQLHANDLE m_sqlStmtHandle;
	std::unordered_map<std::string, int>m_grades;
};

