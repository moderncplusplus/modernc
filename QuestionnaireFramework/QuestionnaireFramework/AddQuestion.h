#pragma once

#include <QWidget>
#include "ui_AddQuestion.h"
#include "Database.h"
#include <tuple>

class AddQuestion : public QWidget
{
	Q_OBJECT

public:
	AddQuestion(QWidget* parent = Q_NULLPTR);
	~AddQuestion();
	std::string getCourse()const;
	std::string getAnswer()const;

	bool verifyQuestion(std::string& question_text, std::vector<std::string>& answers, std::string& correct_answer) const;

	void insertQuestion();
	void insertAnswer();

	void printQuestionAndAnswers()const;

	SQLHANDLE getStmtHandle() const;

	void setStmtHandle(const SQLHANDLE& handle);

private	slots:
	void on_pushButton_pressed();

private:
	Ui::AddQuestion ui;
	SQLHANDLE m_sqlStmtHandle;

	int m_question_id;
	std::string m_question_text;
	int m_chapter;
	std::string m_course;

	std::tuple< std::string, std::string, std::string, std::string> m_answers;
	std::string m_answer;

};