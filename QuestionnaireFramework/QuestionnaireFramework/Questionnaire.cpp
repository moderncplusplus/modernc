#include "Questionnaire.h"
#include <sstream>
#include "Question.h"
#include "Answer.h"

#define SQL_RESULT_LEN 1000

Questionnaire::Questionnaire(const std::string subject, SQLHANDLE sqlStmtHandle)
{
	m_subject = subject;
	m_sqlStmtHandle = sqlStmtHandle;
}

void Questionnaire::chooseQuestions()
{
	m_nrChapters = 0;
	m_questions.clear();

	SQLHANDLE sqlStmtHandle = m_sqlStmtHandle;
	SQLFreeStmt(sqlStmtHandle, SQL_CLOSE);
	std::string query_string = "SELECT question_id, question_text, chapter  FROM Questions WHERE subject='" + m_subject + "'";
	std::wstring  query_wstring(query_string.begin(), query_string.end());
	SQLWCHAR* SQLQuery = (SQLWCHAR*)query_wstring.c_str();

	LoggerSingleton::getLoggerInstance().log("Executing T-SQL Question choose query...\n", Logger::Level::Info);

	//if there is a problem executing the query then exit application else display query result
	if (SQL_SUCCESS != SQLExecDirect(sqlStmtHandle, SQLQuery, SQL_NTS)) {
		LoggerSingleton::getLoggerInstance().log("Error querying SQL Server\n", Logger::Level::Error);
	}
	else {
		int question_id;
		char  question_text[SQL_RESULT_LEN];
		int  chapter;

		while (SQLFetch(m_sqlStmtHandle) == SQL_SUCCESS) {
			// Fetches the next rowset of data from the result
			SQLGetData(m_sqlStmtHandle, 1, SQL_C_DEFAULT, &question_id, sizeof(question_id), NULL);
			SQLGetData(m_sqlStmtHandle, 2, SQL_C_DEFAULT, &question_text, sizeof(question_text), NULL);
			SQLGetData(m_sqlStmtHandle, 3, SQL_C_DEFAULT, &chapter, sizeof(chapter), NULL);

			if (chapter > m_nrChapters)
				m_nrChapters = chapter;

			Question question(question_text, question_id, chapter);
			m_questions.emplace_back(question);

		}
		SQLFreeStmt(sqlStmtHandle, SQL_CLOSE);
	}
	questionSelect();
	chapterSelect();
}

void Questionnaire::writeQuestions()
{
	LoggerSingleton::getLoggerInstance().log("QUESTIONS\n=====================================================================\n", Logger::Level::Info);
	if (m_questions.size() > 0)
		for (auto& question : m_questions)
		{
			LoggerSingleton::getLoggerInstance().log("Intrebare: " + question.getQuestion() + "\n", Logger::Level::Info);
		}
	else LoggerSingleton::getLoggerInstance().log("Nothing to write!\n", Logger::Level::Warning);
}

void Questionnaire::writeAnswers()
{

	LoggerSingleton::getLoggerInstance().log("ANSWERS\n=====================================================================\n", Logger::Level::Info);
	if (!m_questions.empty())
	{
		for (auto& answer : m_answers)
		{
			LoggerSingleton::getLoggerInstance().log(std::to_string(answer.getId()) + " "
				+ std::to_string(answer.getQuestionId()) + "\n"
				+ answer.getAnswer(1) + "\n" + answer.getAnswer(2) + "\n"
				+ answer.getAnswer(3) + "\n" + answer.getAnswer(4) + "\n"
				+ answer.getCorrectAnswer() + "\n", Logger::Level::Info);
		}
	}
	else LoggerSingleton::getLoggerInstance().log("Nothing to write!\n", Logger::Level::Warning);
}

void Questionnaire::chooseAnswers()
{
	m_answers.clear();

	SQLHANDLE sqlStmtHandle = m_sqlStmtHandle;
	SQLFreeStmt(sqlStmtHandle, SQL_CLOSE);
	std::string query_string = "SELECT * FROM Answer WHERE question_id=";

	for (auto& question : m_questions)
	{
		LoggerSingleton::getLoggerInstance().log("QUESTION ID: " + std::to_string(question.getQuestionId()) + "\n", Logger::Level::Info);
		std::string query_string = "SELECT * FROM Answer WHERE question_id=" + std::to_string(question.getQuestionId());
		LoggerSingleton::getLoggerInstance().log(query_string + "\n", Logger::Level::Info);
		std::wstring  query_wstring(query_string.begin(), query_string.end());
		SQLWCHAR* SQLQuery = (SQLWCHAR*)query_wstring.c_str();

		LoggerSingleton::getLoggerInstance().log("Executing T-SQL Answer choose query...\n", Logger::Level::Info);

		if (SQL_SUCCESS != SQLExecDirect(sqlStmtHandle, SQLQuery, SQL_NTS))
		{
			LoggerSingleton::getLoggerInstance().log("Error querying SQL Server\n", Logger::Level::Error);
		}
		else {

			int answerId, questionId;
			char answerA[SQL_RESULT_LEN];
			char answerB[SQL_RESULT_LEN];
			char answerC[SQL_RESULT_LEN];
			char answerD[SQL_RESULT_LEN];
			char studentAnswer[SQL_RESULT_LEN];


			while (SQLFetch(m_sqlStmtHandle) == SQL_SUCCESS)
			{

				SQLGetData(m_sqlStmtHandle, 1, SQL_C_DEFAULT, &answerId, sizeof(answerId), NULL);
				SQLGetData(m_sqlStmtHandle, 2, SQL_C_DEFAULT, &questionId, sizeof(questionId), NULL);
				SQLGetData(m_sqlStmtHandle, 3, SQL_C_DEFAULT, &answerA, sizeof(answerA), NULL);
				SQLGetData(m_sqlStmtHandle, 4, SQL_C_DEFAULT, &answerB, sizeof(answerB), NULL);
				SQLGetData(m_sqlStmtHandle, 5, SQL_C_DEFAULT, &answerC, sizeof(answerC), NULL);
				SQLGetData(m_sqlStmtHandle, 6, SQL_C_DEFAULT, &answerD, sizeof(answerD), NULL);
				SQLGetData(m_sqlStmtHandle, 7, SQL_C_DEFAULT, &studentAnswer, sizeof(studentAnswer), NULL);

				Answer answer(answerId, questionId, studentAnswer);
				answer.setAllAnswers(answerA, answerB, answerC, answerD);
				m_answers.emplace_back(answer);
			}

			SQLFreeStmt(sqlStmtHandle, SQL_CLOSE);
		}
	}
}


void Questionnaire::setSubject(const std::string& subject)
{
	m_subject = subject;
}

void Questionnaire::setSqlStmtHandle(const SQLHANDLE& sqlStmtHandle)
{
	m_sqlStmtHandle = sqlStmtHandle;
}

std::vector<Question> Questionnaire::getQuestions()
{
	if (m_questions.empty())
	{
		this->chooseQuestions();
	}

	return m_questions;
}

std::vector<Answer> Questionnaire::getAnswer()
{
	if (m_questions.empty())
	{
		this->chooseAnswers();
	}

	return m_answers;
}

void Questionnaire::questionSelect()
{
	m_selectedQuestions.clear();
	m_selectedQuestions.resize(m_nrChapters);
	for (int index = 0; index < m_questions.size(); index++)
	{
		m_selectedQuestions.at(m_questions.at(index).getChapter() - 1).push_back(m_questions.at(index));
	}
}

void Questionnaire::chapterSelect()
{
	std::vector<Question> auxQuestion;
	int currentIndex = 0;
	int chapterPerQuestion = MAXQUESTIONS / m_nrChapters;
	std::random_device r;
	std::seed_seq seed{ r(), r(), r(), r(), r(), r(), r(), r() };

	std::mt19937 engine(seed);

	for (int index_i = 0; index_i < m_selectedQuestions.size(); ++index_i)
	{
		int contor = m_selectedQuestions.at(index_i).size();
		for (int index_j = 0; index_j < chapterPerQuestion; index_j++)
		{
			if (contor > 0)
			{
				std::uniform_int_distribution<int> gen(0, m_selectedQuestions.at(index_i).size() - 1);
				int questionSelector = gen(engine);
				auxQuestion.emplace_back(m_selectedQuestions.at(index_i).at(questionSelector));
				m_selectedQuestions.at(index_i).erase(m_selectedQuestions.at(index_i).begin() + questionSelector);
				--contor;
			}
		}
	}
	while (auxQuestion.size() < MAXQUESTIONS)
	{
		std::uniform_int_distribution<int> chapterGenerator(0, m_selectedQuestions.size() - 1);
		int chapterRandomSelector = chapterGenerator(engine);
		if (m_selectedQuestions.at(chapterRandomSelector).size() > 0)
		{
			std::uniform_int_distribution<int> QuestionGenerator(0, m_selectedQuestions.at(chapterRandomSelector).size() - 1);
			int QuestionRandomSelector = QuestionGenerator(engine);
			auxQuestion.emplace_back(m_selectedQuestions.at(chapterRandomSelector).at(QuestionRandomSelector));
			m_selectedQuestions.at(chapterRandomSelector).erase(m_selectedQuestions.at(chapterRandomSelector).begin() + QuestionRandomSelector);
		}
	}
	m_questions.clear();
	m_questions = std::move(auxQuestion);
}
