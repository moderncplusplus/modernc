#pragma once
#include <string>
#include "Database.h"
#include "Question.h"
#include "Answer.h"
#include <random>

class Questionnaire
{
public:
	Questionnaire(const std::string subject, SQLHANDLE sqlStmtHandle);
	Questionnaire() = default;
	void chooseQuestions();
	void chooseAnswers();

	// Temporary method
	void writeQuestions();
	void writeAnswers();
	void setSubject(const std::string& subject);
	void setSqlStmtHandle(const SQLHANDLE& sqlStmtHandle);

	void questionSelect();
	void chapterSelect();

	std::vector<Question> getQuestions();
	std::vector<Answer> getAnswer();

private:
	std::string m_subject;
	SQLHANDLE m_sqlStmtHandle;
	std::vector<Question> m_questions;
	std::vector<Answer> m_answers;

	std::vector<std::vector<Question>> m_selectedQuestions;
	std::vector<std::pair<int, int>> m_chapterCapacity; //first->chapter, second->capacity
	int m_nrChapters;

	const int MAXQUESTIONS = 20;
};

