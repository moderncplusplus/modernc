#include "QuestionnaireFramework.h"
#include <QtWidgets/qmessagebox.h>
#include "Networking.h"
#include <iostream>
#include <sstream>
#include <QMediaPlayer>
#include "PasswordHash.h"
#include <regex>
#include <random>
#include <exception>

#define SQL_RESULT_LEN 50
#define CAPTCHA_LENGTH 9
#define SQL_PASS_LEN 100

std::string gen_captcha() {
	char* chrs = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789"; // l si I sunt scoase pentru ca nu se deosebesc
	std::string captcha = "";
	int n = CAPTCHA_LENGTH;
	while (n--)
	{
		std::random_device device;
		std::mt19937 generator(device());

		std::uniform_int_distribution<int> distribution(0, 60);
		captcha.push_back(chrs[distribution(generator)]);
	}
	return captcha;
}

bool QuestionnaireFramework::check_capatcha(const QString& newString)
{
	return QString::compare(m_captcha, newString, Qt::CaseInsensitive) == 0;
}

QuestionnaireFramework::QuestionnaireFramework(QWidget* parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	m_captcha = QString::fromStdString(gen_captcha());
	ui.capatcha_text_info->setText("Captcha: " + m_captcha);
}

void QuestionnaireFramework::on_loginButton_pressed()
{
	QString username = ui.lineEdit_username->text();
	QString password = ui.lineEdit_password->text();

	QString captcha_text_info = ui.capatcha_text_info->text();
	QString captcha_edit_line = ui.code_input->text();

	if (!check_capatcha(captcha_edit_line))
	{
		QMessageBox::warning(this, "Captcha gresit!", "Verificarea captcha a esuat! Incercati din nou!");
		m_captcha = QString::fromStdString(gen_captcha());
		ui.capatcha_text_info->setText("Captcha: " + m_captcha);
		ui.code_input->setText("");
		LoggerSingleton::getLoggerInstance().log("Verificarea captcha a esuat! Incercati din nou!\n", Logger::Level::Warning);
		return;
	}

	setUsername(username);
	setPassword(password);
	if (ui.professorCheck->checkState() == Qt::Unchecked)
		verifyStudentAccount();
	else
		verifyTeacherAccount();

	PasswordHash p;
	p.testMethod();
}


void QuestionnaireFramework::on_registerButton_pressed()
{
	regFrame.setStmt(database.getStmtHandle());
	regFrame.show();

}


void QuestionnaireFramework::showEvent(QShowEvent* ev)
{
	QMainWindow::showEvent(ev);
	//Verify if is possible to connect to the database. Show a warning message if not possbile and exit the application.
	Networking networking;
	if (!networking.networkConnection_QuickMode())
	{
		if (!networking.networkConnection_AdvancedMode())
		{
			QMessageBox::warning(this, "Eroare de conexiune", "A aparut o eroare privind conexiunea la internet. Va rugam sa verificati conexiunea dvs. la internet!");
			exit(EXIT_FAILURE);
		}
	}
	if (!database.getIsConnected())
	{
		QMessageBox::warning(this, "Eroare de conexiune", "A aparut o eroare privind conectarea la baza de date. Va rugam sa reveniti mai tarziu");
		exit(EXIT_FAILURE);
	}
}

void QuestionnaireFramework::setUsername(const QString& username)
{
	m_username = username;
}

void QuestionnaireFramework::setPassword(const QString& password)
{
	m_password = password;
}

void QuestionnaireFramework::verifyStudentAccount()
{
	SQLHANDLE sqlStmtHandle = database.getStmtHandle();
	m_student.setHandle(database.getStmtHandle());
	SQLWCHAR SQLQuery[] = L"SELECT * FROM Student";

	std::string user_username = m_username.toUtf8().constData();
	std::string user_password = m_password.toUtf8().constData();

	std::string data_username;
	std::string data_password;

	LoggerSingleton::getLoggerInstance().log("Executing T-SQL Student account query...\n", Logger::Level::Info);
	//if there is a problem executing the query then exit application else display query result
	if (SQL_SUCCESS != SQLExecDirect(sqlStmtHandle, SQLQuery, SQL_NTS)) {
		LoggerSingleton::getLoggerInstance().log("Error querying SQL Server\n", Logger::Level::Error);
	}

	else {

		char  username[SQL_RESULT_LEN];
		char  password[SQL_PASS_LEN];
		int id;
		bool connect = false;

		while (SQLFetch(sqlStmtHandle) == SQL_SUCCESS) {
			// Fetches the next rowset of data from the result
			SQLGetData(sqlStmtHandle, 1, SQL_C_DEFAULT, &id, sizeof(id), NULL);
			SQLGetData(sqlStmtHandle, 2, SQL_C_DEFAULT, &username, sizeof(username), NULL);
			SQLGetData(sqlStmtHandle, 3, SQL_C_DEFAULT, &password, sizeof(password), NULL);

			std::stringstream ss;
			ss << username;
			ss >> data_username;
			ss.clear();
			ss << password;
			ss >> data_password;

			std::string whitespace = " ";
			for (char c : whitespace)
			{
				data_password.erase(std::remove(data_password.begin(), data_password.end(), c), data_password.end());
			}

			LoggerSingleton::getLoggerInstance().log("user pass: " + data_password + "\n", Logger::Level::Info);

			//Verify if the user exists in the database 
			if (data_username == user_username && checkPasswordHash(user_password, data_password)) {
				//If exists, set the id of the student to retrieve additional information about him from the database
				m_student.setId(id);
				m_student.setHandle(database.getStmtHandle());
				m_mainMenu.setStudent(std::forward<Student&&>(m_student));
				m_mainMenu.show();
				playSound();
				close();
				connect = true;
				break;
			}
		}
		if (connect == false)
		{
			QMessageBox::warning(this, "Autentificare", "Numele de utilizator si/sau parola nu sunt corecte!");
			m_captcha = QString::fromStdString(gen_captcha());
			ui.capatcha_text_info->setText("Captcha: " + m_captcha);
			ui.code_input->setText("");
		}
	}
	SQLFreeStmt(sqlStmtHandle, SQL_CLOSE);
}

void QuestionnaireFramework::verifyTeacherAccount()
{
	SQLHANDLE sqlStmtHandle = database.getStmtHandle();
	m_student.setHandle(database.getStmtHandle());
	SQLWCHAR SQLQuery[] = L"SELECT * FROM Teacher";

	std::string user_username = m_username.toUtf8().constData();
	std::string user_password = m_password.toUtf8().constData();

	std::string data_username;
	std::string data_password;

	LoggerSingleton::getLoggerInstance().log("Executing T-SQL Teacher account query...\n", Logger::Level::Info);

	//if there is a problem executing the query then exit application else display query result
	if (SQL_SUCCESS != SQLExecDirect(sqlStmtHandle, SQLQuery, SQL_NTS)) {
		LoggerSingleton::getLoggerInstance().log("Error querying SQL Server\n", Logger::Level::Error);
	}

	else {

		char  username[SQL_RESULT_LEN];
		char  password[SQL_PASS_LEN];
		int id;
		bool connect = false;

		while (SQLFetch(sqlStmtHandle) == SQL_SUCCESS) {
			// Fetches the next rowset of data from the result
			SQLGetData(sqlStmtHandle, 1, SQL_C_DEFAULT, &username, sizeof(username), NULL);
			SQLGetData(sqlStmtHandle, 2, SQL_C_DEFAULT, &password, sizeof(password), NULL);
			SQLGetData(sqlStmtHandle, 3, SQL_C_DEFAULT, &id, sizeof(id), NULL);

			std::stringstream ss;
			ss << username;
			ss >> data_username;
			ss.clear();
			ss << password;
			ss >> data_password;

			std::string whitespace = " ";
			for (char c : whitespace)
			{
				data_password.erase(std::remove(data_password.begin(), data_password.end(), c), data_password.end());
			}

			//Verify if the user exists in the database 
			if (data_username == user_username && checkPasswordHash(user_password, data_password)) {
				//If exists, set the id of the student to retrieve additional information about him from the database
				m_AddQuestion.setStmtHandle(database.getStmtHandle());
				m_AddQuestion.show();
				playSound();
				close();
				connect = true;
				break;
			}
		}
		if (connect == false)
		{
			QMessageBox::warning(this, "Autentificare", "Numele de utilizator si/sau parola nu sunt corecte!");
			m_captcha = QString::fromStdString(gen_captcha());
			ui.capatcha_text_info->setText("Captcha: " + m_captcha);
			ui.code_input->setText("");
		}
	}
	SQLFreeStmt(sqlStmtHandle, SQL_CLOSE);
}

void QuestionnaireFramework::playSound()
{
	QMediaPlayer* soundPlayer = new QMediaPlayer();
	soundPlayer->setMedia(QUrl("qrc:/new/sounds/shiny_login.mp3"));
	soundPlayer->setVolume(45);
	soundPlayer->play();
}

bool QuestionnaireFramework::checkPasswordHash(const std::string& password, const std::string& hashPassword)
{
	PasswordHash pHash;
	const char* pass = password.c_str();
	const char* hash = hashPassword.c_str();

	try {

		bool checkValid = pHash.verifyPassword(pass, hash);
		return checkValid;
	}
	catch (std::exception& exception)
	{
		LoggerSingleton::getLoggerInstance().log("Error during password verification" + std::string(exception.what()) + "\n", Logger::Level::Error);
		return false;
	}
}

QString QuestionnaireFramework::getUsername() const
{
	return m_username;
}

QString QuestionnaireFramework::getPassword() const
{
	return m_password;
}
