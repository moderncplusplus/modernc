#pragma once

#include <String>
#include <iostream>
#include "Answer.h"
#include <vector>

class Question
{
public:

	Question();
	Question(const std::string& question_text, int question_id, int chapter);
	~Question();

	std::string getQuestion() const;
	void setQuestion(const std::string&, int, int);

	Answer getCorrectAnswer() const;
	void setCorrectAnswer(const Answer&);

	std::vector<Answer> getAllAnswers() const;
	void setPossibleAnswers(const std::vector<Answer>&);

	bool checkAnswer(const Answer&) const;
	float questionScoreCalc(const Answer&) const;

	int getQuestionId() const;
	int getChapter() const;

	friend std::ostream& operator<<(std::ostream&, const Question&);

private:

	std::string m_question_text;
	int m_question_id;
	int m_chapter;

	Answer m_correctAnswer;
	std::vector<Answer> m_possibleAnswers;

};

