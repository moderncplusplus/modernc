#include "registerFrame.h"
#include <iostream>
#include <sql.h>
#include <QtWidgets/qmessagebox.h>
#include "PasswordHash.h"
#include <exception>

#define SQL_RESULT_LEN 50

RegisterFrame::RegisterFrame(QWidget* parent)
	: QWidget(parent)
{
	ui.setupUi(this);
}

RegisterFrame::~RegisterFrame()
{
}


void RegisterFrame::on_inregistrareButton_pressed()
{
	LoggerSingleton::getLoggerInstance().log("Register button pressed!\n", Logger::Level::Info);
	setFields();
	if (valideFields())
	{
		if (!checkIfUserExists() && !checkIfMailExists())
		{
			updateStudentTable();
			updateStudentDetailsTable();
			updateGradesTable();
			this->setVisible(false);
		}
	}
}

void RegisterFrame::setStmt(const SQLHANDLE& stmt)
{
	m_stmt = stmt;
}

void RegisterFrame::setFields()
{
	m_usernameStudent = ui.lineEdit->text().toUtf8().constData();
	m_passwordStudent = ui.lineEdit_2->text().toUtf8().constData();
	m_emailStudent = ui.lineEdit_3->text().toUtf8().constData();
	m_groupStudent = ui.lineEdit_4->text().toUtf8().constData();
	m_specializationStudent = ui.lineEdit_5->text().toUtf8().constData();
	m_facultyStudent = ui.lineEdit_6->text().toUtf8().constData();
}

bool RegisterFrame::valideFields()
{
	if (m_usernameStudent.empty())
	{
		QMessageBox::warning(this, "Eroare la inregistrare", "Campul 'Nume de utilizator student' este gol. Va rugam sa verificati acest camp!");
		return false;
	}
	if (m_passwordStudent.empty())
	{
		QMessageBox::warning(this, "Eroare la inregistrare", "Campul 'Parola student' este gol. Va rugam sa verificati acest camp!");
		return false;
	}
	if (m_emailStudent.empty())
	{
		QMessageBox::warning(this, "Eroare la inregistrare", "Campul 'Email student' este gol. Va rugam sa verificati acest camp!");
		return false;
	}
	if (m_emailStudent.find('@') == std::string::npos || m_emailStudent.find('.') == std::string::npos)
	{
		QMessageBox::warning(this, "Eroare la inregistrare", "Campul 'Email student' nu reprezinta un email valid. Va rugam sa verificati acest camp!");
		return false;
	}
	if (m_groupStudent.empty())
	{
		QMessageBox::warning(this, "Eroare la inregistrare", "Campul 'Grupa student' este gol. Va rugam sa verificati acest camp!");
		return false;
	}
	if (m_specializationStudent.empty())
	{
		QMessageBox::warning(this, "Eroare la inregistrare", "Campul 'Specializare student' este gol. Va rugam sa verificati acest camp!");
		return false;
	}
	if (m_facultyStudent.empty())
	{
		QMessageBox::warning(this, "Eroare la inregistrare", "Campul 'Facultate student' este gol. Va rugam sa verificati acest camp!");
		return false;
	}
	return true;
}

void RegisterFrame::updateStudentTable()
{
	const char* pass = m_passwordStudent.c_str();
	std::string encryptedPass = hashPassword(pass);

	LoggerSingleton::getLoggerInstance().log("Executing T-SQL Student insert query...\n", Logger::Level::Info);

	SQLRETURN retcode = SQLPrepare(m_stmt, (SQLWCHAR*)L"INSERT INTO Student (username, password) OUTPUT Inserted.student_id VALUES(?,?) ", SQL_NTS);
	retcode = SQLBindParameter(m_stmt, 1, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 50, 0, (SQLPOINTER)m_usernameStudent.c_str(), m_usernameStudent.length(), NULL);
	retcode = SQLBindParameter(m_stmt, 2, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 100, 0, (SQLPOINTER)encryptedPass.c_str(), encryptedPass.length(), NULL);
	retcode = SQLExecute(m_stmt);

	if (SQL_SUCCESS != retcode) {
		LoggerSingleton::getLoggerInstance().log("Error querying SQL Server\n", Logger::Level::Error);
	}
	else
	{
		while (SQLFetch(m_stmt) == SQL_SUCCESS) {

			SQLGetData(m_stmt, 1, SQL_C_DEFAULT, &m_student_id, sizeof(m_student_id), NULL);
		}

		SQLFreeStmt(m_stmt, SQL_CLOSE);
	}
}

void RegisterFrame::updateStudentDetailsTable()
{
	LoggerSingleton::getLoggerInstance().log("Executing T-SQL Student_Details insert query...\n", Logger::Level::Info);

	SQLRETURN retcode = SQLPrepare(m_stmt, (SQLWCHAR*)L"INSERT INTO Student_Details (id, email, grupa, specializare, facultate) VALUES(?,?,?,?,?) ", SQL_NTS);
	retcode = SQLBindParameter(m_stmt, 1, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &m_student_id, 0, NULL);
	retcode = SQLBindParameter(m_stmt, 2, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 50, 0, (SQLPOINTER)m_emailStudent.c_str(), m_emailStudent.length(), NULL);
	retcode = SQLBindParameter(m_stmt, 3, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 50, 0, (SQLPOINTER)m_groupStudent.c_str(), m_groupStudent.length(), NULL);
	retcode = SQLBindParameter(m_stmt, 4, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 50, 0, (SQLPOINTER)m_specializationStudent.c_str(), m_specializationStudent.length(), NULL);
	retcode = SQLBindParameter(m_stmt, 5, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, 50, 0, (SQLPOINTER)m_facultyStudent.c_str(), m_facultyStudent.length(), NULL);
	retcode = SQLExecute(m_stmt);

	if (SQL_SUCCESS != retcode) {
		LoggerSingleton::getLoggerInstance().log("Error querying SQL Server\n", Logger::Level::Error);
	}
	SQLFreeStmt(m_stmt, SQL_CLOSE);
}

void RegisterFrame::updateGradesTable()
{
	LoggerSingleton::getLoggerInstance().log("Executing T-SQL Grade insert query...\n", Logger::Level::Info);

	SQLRETURN retcode = SQLPrepare(m_stmt, (SQLWCHAR*)L"INSERT INTO Grades (student_id) VALUES(?) ", SQL_NTS);
	retcode = SQLBindParameter(m_stmt, 1, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &m_student_id, 0, NULL);
	retcode = SQLExecute(m_stmt);

	if (SQL_SUCCESS != retcode) {
		LoggerSingleton::getLoggerInstance().log("Error querying SQL Server\n", Logger::Level::Error);
	}
	SQLFreeStmt(m_stmt, SQL_CLOSE);
}

bool RegisterFrame::checkIfUserExists()
{
	bool foundUserName = false;

	SQLHANDLE sqlStmtHandle = m_stmt;
	SQLFreeStmt(sqlStmtHandle, SQL_CLOSE);

	std::string query_string = "SELECT * FROM Student";
	std::wstring  query_wstring(query_string.begin(), query_string.end());
	SQLWCHAR* SQLQuery = (SQLWCHAR*)query_wstring.c_str();

	LoggerSingleton::getLoggerInstance().log("Executing T-SQL Student select query...\n", Logger::Level::Info);

	if (SQL_SUCCESS != SQLExecDirect(sqlStmtHandle, SQLQuery, SQL_NTS))
	{
		LoggerSingleton::getLoggerInstance().log("Error querying SQL Server\n", Logger::Level::Error);
	}
	else
	{

		char dataUserName[SQL_RESULT_LEN];
		char dataPassword[SQL_RESULT_LEN];

		while (SQLFetch(sqlStmtHandle) == SQL_SUCCESS)
		{
			SQLGetData(sqlStmtHandle, 2, SQL_C_DEFAULT, &dataUserName, sizeof(dataUserName), NULL);
			SQLGetData(sqlStmtHandle, 3, SQL_C_DEFAULT, &dataPassword, sizeof(dataPassword), NULL);

			int stringSize = sizeof(dataUserName) / sizeof(char);
			std::string dataUsr(dataUserName);
			std::string dataPass(dataPassword);

			LoggerSingleton::getLoggerInstance().log("Data User: " + dataUsr + "\n", Logger::Level::Info);
			LoggerSingleton::getLoggerInstance().log("Data Password: " + dataPass + "\n", Logger::Level::Info);

			if (dataUsr == m_usernameStudent)
			{
				LoggerSingleton::getLoggerInstance().log("Exista deja un user cu acest nume in baza de date! Va rugam sa alegeti alt nume de utilizator\n", Logger::Level::Info);
				QMessageBox::warning(this, "Eroare la inregistrare", "Usernameul selectat este deja folosit! Va rugam sa introduceti alt nume de utilizator! ");

				foundUserName = true;
				break;
			}

		}

		SQLFreeStmt(sqlStmtHandle, SQL_CLOSE);
	}

	if (foundUserName)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool RegisterFrame::checkIfMailExists()
{
	bool foundUserMail = false;

	SQLHANDLE sqlStmtHandle = m_stmt;
	SQLFreeStmt(sqlStmtHandle, SQL_CLOSE);

	std::string query_string = "SELECT * FROM Student_details";
	std::wstring  query_wstring(query_string.begin(), query_string.end());
	SQLWCHAR* SQLQuery = (SQLWCHAR*)query_wstring.c_str();

	LoggerSingleton::getLoggerInstance().log("Executing T-SQL Student_details select query...\n", Logger::Level::Info);

	if (SQL_SUCCESS != SQLExecDirect(sqlStmtHandle, SQLQuery, SQL_NTS))
	{
		LoggerSingleton::getLoggerInstance().log("Error querying SQL Server\n", Logger::Level::Error);
	}
	else
	{
		int userId;
		char dataUserMail[SQL_RESULT_LEN];


		while (SQLFetch(sqlStmtHandle) == SQL_SUCCESS)
		{
			SQLGetData(sqlStmtHandle, 1, SQL_C_DEFAULT, &userId, sizeof(userId), NULL);
			SQLGetData(sqlStmtHandle, 2, SQL_C_DEFAULT, &dataUserMail, sizeof(dataUserMail), NULL);

			int stringSize = sizeof(dataUserMail) / sizeof(char);
			std::string dataMail(dataUserMail);

			LoggerSingleton::getLoggerInstance().log("User Mail: " + dataMail + "\n", Logger::Level::Info);
			LoggerSingleton::getLoggerInstance().log("User ID: " + std::to_string(userId) + "\n", Logger::Level::Info);


			if (dataMail == m_emailStudent)
			{
				LoggerSingleton::getLoggerInstance().log("Mailul introdus este deja folosit.\n", Logger::Level::Info);
				QMessageBox::warning(this, "Eroare la inregistrare", "Adresa de email introdusa este deja folosita! Va rugam sa selectati o alta adresa de email.\n");

				foundUserMail = true;
				break;
			}

		}

		SQLFreeStmt(sqlStmtHandle, SQL_CLOSE);
	}

	if (foundUserMail)
	{
		return true;
	}
	else
	{
		return false;
	}
}

std::string RegisterFrame::hashPassword(const char*& userPass)
{
	std::string resultHash = "default hash";
	PasswordHash pHash;
	const char* bCryptPass;

	try {

		pHash.hashPassword(userPass, bCryptPass);

		if (bCryptPass != NULL)
		{
			resultHash = std::string(bCryptPass);
		}
	}
	catch (std::exception& exception)
	{
		LoggerSingleton::getLoggerInstance().log("Hashing error" + std::string(exception.what()) + "\n", Logger::Level::Error);

	}

	return resultHash;

}

