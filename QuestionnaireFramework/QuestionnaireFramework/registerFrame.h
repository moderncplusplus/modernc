#pragma once

#include <QWidget>
#include <QFileDialog>
#include "ui_RegisterFrame.h"
#include "Database.h"

class RegisterFrame : public QWidget
{
	Q_OBJECT

public:
	RegisterFrame(QWidget* parent = Q_NULLPTR);
	~RegisterFrame();

public:
	void setStmt(const SQLHANDLE& stmt);

private slots:
	void on_inregistrareButton_pressed();

private:
	void setFields();
	bool valideFields();
	void updateStudentTable();
	void updateStudentDetailsTable();
	void updateGradesTable();

	bool checkIfUserExists();
	bool checkIfMailExists();
	
	std::string hashPassword(const char*&);

private:
	Ui::RegisterFrame ui;

private:
	SQLHANDLE m_stmt;
	std::string m_usernameStudent;
	std::string m_passwordStudent;
	std::string m_emailStudent;
	std::string m_groupStudent;
	std::string m_specializationStudent;
	std::string m_facultyStudent;
	int m_student_id;
};
