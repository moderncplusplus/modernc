#include "CourseFrame.h" 
#include <random>
#include <sstream>
#include <string>
#include <math.h>
#include <iterator>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include <QMediaPlayer>
#include <fstream>

#define SQL_RESULT_LEN 100

CourseFrame::CourseFrame(QWidget* parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	timer = new QTimer();
	connect(timer, SIGNAL(timeout()), this, SLOT(updateCountdown()));
	timer->start(1000);
	ui.stackedWidget->setCurrentIndex(0);

	connect(ui.answer1, SIGNAL(clicked(bool)), this, SLOT(playSound()));
	connect(ui.answer2, SIGNAL(clicked(bool)), this, SLOT(playSound()));
	connect(ui.answer3, SIGNAL(clicked(bool)), this, SLOT(playSound()));
	connect(ui.answer4, SIGNAL(clicked(bool)), this, SLOT(playSound()));
	connect(ui.next_button, SIGNAL(pressed()), this, SLOT(playSound()));
	connect(ui.previous_button, SIGNAL(pressed()), this, SLOT(playSound()));
	connect(ui.finish_button, SIGNAL(pressed()), this, SLOT(playSound()));
}

CourseFrame::~CourseFrame()
{
}


void CourseFrame::updateCountdown()
{
	time = time.addSecs(-1);
	ui.time_remaining->setText(time.toString("mm:ss"));
	if (ui.time_remaining->text() == "00:01")
	{
		timer->stop();
	}

	if (ui.time_remaining->text() == "00:10")
	{
		QMediaPlayer* soundPlayer = new QMediaPlayer();
		soundPlayer->setMedia(QUrl("qrc:/new/sounds/countdown_sound.mp3"));
		soundPlayer->setVolume(50);
		soundPlayer->play();
	}
}

void CourseFrame::addQuestionsToFrame(const std::vector<Question>& questionsList)
{
	this->m_questions = questionsList;
}

void CourseFrame::addAnswersToFrame(const std::vector<Answer>& answersList)
{
	this->m_answers = answersList;
}

void CourseFrame::setStudent(const Student& student)
{
	m_student = student;
}

void CourseFrame::setCourseSubject(const std::string& subject)
{
	m_courseSubject = subject;
}

void CourseFrame::setIsMuteFalse()
{
	m_isMuted = false;
}

void CourseFrame::setIsMuteTrue()
{
	m_isMuted = true;

}

void CourseFrame::setSoundOff()
{
	ui.sound_text->setText("   OPRIT");
}

void CourseFrame::setSoundOn()
{
	ui.sound_text->setText("   PORNIT");
}

void CourseFrame::setBGColor(QString color)
{
	this->setStyleSheet(color);
	ui.page1->setStyleSheet(color);
	ui.page2->setStyleSheet(color);
	ui.page3->setStyleSheet(color);
}

int CourseFrame::getGrade()
{
	return (int)m_finalGrade;
}

void CourseFrame::on_start_button_pressed()
{

	ui.stackedWidget->setCurrentIndex(1);

	time.setHMS(0, 20, 0);
	shuffleQuestionsAnswers();
	m_startTime = m_email.dateTimeToString(m_email.currentTime(), "%A %B, %d %Y %I:%M%p");

	QString question_text = QString::fromStdString("1.  " + m_questions.at(0).getQuestion());
	ui.question_text->setText(question_text);
	m_currentQuestion = 0;

	ui.answer1->setText(QString::fromStdString(m_answers.at(0).getAnswer(1)));
	ui.answer2->setText(QString::fromStdString(m_answers.at(0).getAnswer(2)));
	ui.answer3->setText(QString::fromStdString(m_answers.at(0).getAnswer(3)));
	ui.answer4->setText(QString::fromStdString(m_answers.at(0).getAnswer(4)));

	m_checkboxState.reserve(20);
	initCheckboxState();
	checkPreviousButton();
}

void CourseFrame::on_finish_button_pressed()
{
	std::string answerToQuestion = checkAnswer();
	m_currentQuestion++;

	if (!answerToQuestion.empty())
	{
		m_answersByStudent.emplace(m_currentQuestion - 1, answerToQuestion);
	}
	else
	{
		m_answersByStudent.emplace(m_currentQuestion - 1, " ");
	}

	ui.stackedWidget->setCurrentIndex(2);
	modifyGrade();
	timer->stop();
}

void CourseFrame::on_main_menu_button_pressed()
{
	close();
}

void CourseFrame::on_next_button_pressed()
{
	if (std::string answerToQuestion = checkAnswer(); m_currentQuestion < MAXQUESTIONS - 1)
	{
		int questionId = m_questions.at(m_currentQuestion).getQuestionId();

		m_currentQuestion++;
		setQuestionText(m_currentQuestion);
		setAnswerText(m_currentQuestion);

		checkPreviousButton();
		checkNextButton();

		if (!answerToQuestion.empty())
		{
			m_answersByStudent.emplace(m_currentQuestion - 1, answerToQuestion);
		}
		else
		{
			m_answersByStudent.emplace(m_currentQuestion - 1, " ");
		}

		clearCheckboxes();
		updateCheckboxState();
	}
	else
	{
		clearCheckboxes();
		updateCheckboxState();
	}
}

void CourseFrame::on_previous_button_pressed()
{
	if (m_currentQuestion > 0)
	{
		std::string answerToQuestion = checkAnswer();
		m_currentQuestion--;
		setQuestionText(m_currentQuestion);
		setAnswerText(m_currentQuestion);

		int questionId = m_questions.at(m_currentQuestion).getQuestionId();

		checkPreviousButton();
		checkNextButton();

		if (!answerToQuestion.empty())
		{
			m_answersByStudent.emplace(m_currentQuestion + 1, answerToQuestion);
		}
		else
		{
			m_answersByStudent.emplace(m_currentQuestion + 1, " ");
		}

		clearCheckboxes();
		updateCheckboxState();
	}
	else
	{
		QMessageBox warningMessage;
		warningMessage.setText("Nu te poti intoarce la intrebarea precedenta!");
		warningMessage.setWindowTitle("Atentie!");
		warningMessage.setIcon(QMessageBox::Warning);
		warningMessage.setFixedHeight(350);
		warningMessage.setFixedWidth(800);
		warningMessage.exec();

		clearCheckboxes();
		updateCheckboxState();
	}
}

void CourseFrame::on_sound_button_pressed()
{
	if (m_isMuted)
	{
		m_isMuted = false;
	}
	else
	{
		m_isMuted = true;
	}

	updateMuteLabel();

}

void CourseFrame::setAnswerText(const int questionNumber)
{
	ui.answer1->setText(QString::fromStdString(m_answers.at(questionNumber).getAnswer(1)));
	ui.answer2->setText(QString::fromStdString(m_answers.at(questionNumber).getAnswer(2)));
	ui.answer3->setText(QString::fromStdString(m_answers.at(questionNumber).getAnswer(3)));
	ui.answer4->setText(QString::fromStdString(m_answers.at(questionNumber).getAnswer(4)));
}

void CourseFrame::setQuestionText(const int questionNumber)
{
	std::string numberOfQuestion = std::to_string(questionNumber + 1) + ".  ";
	QString question_text = QString::fromStdString(numberOfQuestion + m_questions.at(questionNumber).getQuestion());
	ui.question_text->setText(question_text);
}

int CourseFrame::generateRandomNumber()
{
	std::random_device device;
	std::mt19937 generator(device());

	std::uniform_int_distribution<int> distribution(0, MAXQUESTIONS - 1);
	int index = distribution(generator);

	return index;
}

std::string CourseFrame::checkAnswer()
{
	std::stringstream ss;
	std::string answerByStudent = "";

	if (ui.answer1->isChecked())
	{
		ss << 'a';
		std::get<0>(m_checkboxState.at(m_currentQuestion)) = true;
	}
	else
	{
		std::get<0>(m_checkboxState.at(m_currentQuestion)) = false;
	}


	if (ui.answer2->isChecked())
	{
		ss << 'b';
		std::get<1>(m_checkboxState.at(m_currentQuestion)) = true;
	}
	else
	{
		std::get<1>(m_checkboxState.at(m_currentQuestion)) = false;
	}

	if (ui.answer3->isChecked())
	{
		ss << 'c';
		std::get<2>(m_checkboxState.at(m_currentQuestion)) = true;
	}
	else
	{
		std::get<2>(m_checkboxState.at(m_currentQuestion)) = false;
	}

	if (ui.answer4->isChecked())
	{
		ss << 'd';
		std::get<3>(m_checkboxState.at(m_currentQuestion)) = true;
	}
	else
	{
		std::get<3>(m_checkboxState.at(m_currentQuestion)) = false;
	}


	std::get<4>(m_checkboxState.at(m_currentQuestion)) = true;
	ss >> answerByStudent;
	ss.flush();
	return answerByStudent;
}

void CourseFrame::clearCheckboxes()
{
	if (ui.answer1->isChecked())
		ui.answer1->setChecked(false);

	if (ui.answer2->isChecked())
		ui.answer2->setChecked(false);

	if (ui.answer3->isChecked())
		ui.answer3->setChecked(false);

	if (ui.answer4->isChecked())
		ui.answer4->setChecked(false);
}

void CourseFrame::modifyGrade()
{
	double grade = 0;
	int index = 0;
	for (auto current : this->m_answersByStudent)
	{
		int studentAnswerId = current.first;
		std::string correctAnswer = m_answers.at(studentAnswerId).getCorrectAnswer();
		Answer ans = m_answers.at(studentAnswerId);

		std::string whitespace = " ";
		for (char c : whitespace)
		{
			correctAnswer.erase(std::remove(correctAnswer.begin(), correctAnswer.end(), c), correctAnswer.end());
		}

		std::string studentAnswer = current.second;

		double questionDecrement = 0;
		double questionIncrement = 0;
		double questionGrade = 0;

		switch (int maxAnswer = correctAnswer.size(); maxAnswer)
		{
		case 1:
			questionIncrement = 1;
			questionDecrement = 0.33;
			break;
		case 2:
			questionIncrement = 0.5;
			questionDecrement = 0.5;
			break;
		case 3:
			questionIncrement = 0.33;
			questionDecrement = 1;
			break;
		case 4:
			questionIncrement = 0.25;
			questionDecrement = 0;
			break;
		default:
			break;
		}

		for (int indexStudent = 0; indexStudent < studentAnswer.size(); ++indexStudent)
		{
			bool ok = false;
			for (int indexCorect = 0; indexCorect < correctAnswer.size(); ++indexCorect)
			{
				if (studentAnswer[indexStudent] == correctAnswer[indexCorect])
				{
					ok = true;
					break;
				}
			}
			questionGrade += (ok == true) ? questionIncrement : -questionDecrement;

		}
		if (questionGrade > 0.1)
		{
			grade += questionGrade;
		}
		index++;
	}

	m_finalGrade = round((grade * 9) / MAXQUESTIONS + 1);

	ui.grade_text->setText(QString::number(m_finalGrade));

	//Inserting the grade after the questionnaire is finished
	LoggerSingleton::getLoggerInstance().log("La materia " + m_courseSubject + " s-a obtinut nota " + std::to_string(round(m_finalGrade)) + "\n", Logger::Level::Info);
	m_student.setGrade(m_courseSubject, round(m_finalGrade));
	m_student.insertGradeToDatabase(m_courseSubject, round(m_finalGrade));
	emailTextStudentAnswers();
	sendEmail();
}

void CourseFrame::sendEmail()
{
	SQLHANDLE sqlStmtHandle = m_student.getStmtHandle();
	SQLFreeStmt(sqlStmtHandle, SQL_CLOSE);
	std::string query_string = "SELECT teacher_email FROM Teacher WHERE course='" + m_courseSubject + "'";
	std::wstring  query_wstring(query_string.begin(), query_string.end());
	SQLWCHAR* SQLQuery = (SQLWCHAR*)query_wstring.c_str();

	LoggerSingleton::getLoggerInstance().log("Executing T-SQL email choose query...\n", Logger::Level::Info);
	//if there is a problem executing the query then exit application else display query result
	if (SQL_SUCCESS != SQLExecDirect(sqlStmtHandle, SQLQuery, SQL_NTS)) {
		LoggerSingleton::getLoggerInstance().log("Error querying SQL Server\n", Logger::Level::Error);

	}
	else {
		char  teacher_email[SQL_RESULT_LEN];

		while (SQLFetch(sqlStmtHandle) == SQL_SUCCESS) {
			// Fetches the next rowset of data from the result
			SQLGetData(sqlStmtHandle, 1, SQL_C_DEFAULT, &teacher_email, sizeof(teacher_email), NULL);

			m_endTime = m_email.dateTimeToString(m_email.currentTime(), "%A %B, %d %Y %I:%M%p");

			m_email.setName("Questionnaire Server");
			m_email.setAdress(teacher_email);
			m_email.setSubject("Nota chestionar " + m_courseSubject);
			m_email.setMessage("Studentul cu ID-ul " + std::to_string(m_student.getId()) + " a luat nota " + std::to_string((int)round(m_finalGrade)) + ".\n" + "Data la care a inceput chestionarul este: " + m_startTime + "\n" + "Data la care a fost terminat chestionarul este: " + m_endTime);

			if (m_email.sendMail());
		}
		SQLFreeStmt(sqlStmtHandle, SQL_CLOSE);
	}

}

void CourseFrame::initCheckboxState()
{
	auto initValue = std::make_tuple(false, false, false, false, false);
	for (int index = 0; index < MAXQUESTIONS; ++index)
	{
		m_checkboxState.push_back(initValue);
	}
}

void CourseFrame::emailTextStudentAnswers() const
{
	int index = 0;
	std::ofstream file("studentAnswers.txt", std::ios::trunc);

	if (!file)
	{
		LoggerSingleton::getLoggerInstance().log("Error at opening the file", Logger::Level::Error);
	}

	for (auto& checkbox : m_checkboxState)
	{
		file << "Intrebarea: " + std::to_string(++index) + "\n" << m_questions[--index].getQuestion() + "\n";
		file << "Raspunsurile studentului: \n";
		if (std::get<0>(checkbox) != false)
			file << "Raspunsul 1: " + m_answers[index].getAnswer(1) << "\n";
		if (std::get<1>(checkbox) != false)
			file << "Raspunsul 2: " + m_answers[index].getAnswer(2) << "\n";
		if (std::get<2>(checkbox) != false)
			file << "Raspunsul 3: " + m_answers[index].getAnswer(3) << "\n";
		if (std::get<3>(checkbox) != false)
			file << "Raspunsul 4: " + m_answers[index].getAnswer(4) << "\n";
		if (std::get<4>(checkbox) == false)
			file << "Studentul nu a raspuns la intrebare \n";
		file << "\n";
		file << "\n";
		++index;
	}
}
void CourseFrame::checkNextButton()
{
	if (m_currentQuestion == MAXQUESTIONS - 1)
		ui.next_button->setVisible(false);
	else
		ui.next_button->setVisible(true);

}

void CourseFrame::checkPreviousButton()
{
	if (m_currentQuestion == 0)
		ui.previous_button->setVisible(false);
	else
		ui.previous_button->setVisible(true);
}

void CourseFrame::playSound()
{
	QObject* obj = sender();
	QString soundPath;
	if (m_isMuted == false)
	{
		if (obj == ui.answer1 || obj == ui.answer2 || obj == ui.answer3 || obj == ui.answer4)
		{
			soundPath = QString::fromStdString(getSound(Sound::CHECKBOX));
		}

		if (obj == ui.previous_button || obj == ui.next_button)
		{
			soundPath = QString::fromStdString(getSound(Sound::BUTTON_PRESSED));
		}
		if (obj == ui.finish_button)
		{
			soundPath = QString::fromStdString(getSound(Sound::FINAL_TEST));
		}

		QMediaPlayer* soundPlayer = new QMediaPlayer();
		soundPlayer->setMedia(QUrl(soundPath));
		soundPlayer->setVolume(50);
		soundPlayer->play();
	}
}

std::string CourseFrame::getSound(const Sound& sound)
{
	switch (sound)
	{
	case Sound::BUTTON_PRESSED:
		return "qrc:/new/sounds/button_pressed.mp3";
		break;

	case Sound::CHECKBOX:
		return "qrc:/new/sounds/checkbox_sound.mp3";
		break;

	case Sound::COUNTDOWN:
		return "qrc:/new/sounds/countdown_sound.mp3";
		break;

	case Sound::FINAL_TEST:
		return "qrc:/new/sounds/final_test.mp3";
		break;

	case Sound::LOGIN:
		return "qrc:/new/sounds/shiny_login.mp3";
		break;

	default:
		break;
	}
}

void CourseFrame::updateMuteLabel()
{
	if (m_isMuted)
	{
		ui.sound_text->setText("OPRIT");
	}
	else
	{
		ui.sound_text->setText("PORNIT");
	}
}

void CourseFrame::shuffleQuestionsAnswers()
{
	std::random_device r;
	std::seed_seq seed{ r(), r(), r(), r(), r(), r(), r(), r() };

	std::mt19937 eng1(seed);
	auto eng2 = eng1;

	std::shuffle(begin(m_questions), end(m_questions), eng1);
	std::shuffle(begin(m_answers), end(m_answers), eng2);
}


void CourseFrame::updateCheckboxState()
{
	if (std::get<4>(m_checkboxState.at(m_currentQuestion)) == true)
	{
		ui.answer1->setChecked(std::get<0>(m_checkboxState.at(m_currentQuestion)));
		ui.answer2->setChecked(std::get<1>(m_checkboxState.at(m_currentQuestion)));
		ui.answer3->setChecked(std::get<2>(m_checkboxState.at(m_currentQuestion)));
		ui.answer4->setChecked(std::get<3>(m_checkboxState.at(m_currentQuestion)));
	}
	else
	{
		ui.answer1->setChecked(false);
		ui.answer2->setChecked(false);
		ui.answer3->setChecked(false);
		ui.answer4->setChecked(false);

	}
}

