#pragma once
#include <String>
#include <iostream>

class Answer
{

public:

	Answer();
	Answer(const int, const int);
	Answer(const int, const int, const std::string&);
	~Answer();

	std::string getAnswer(const int) const;
	void setAnswer(const std::string&, const int);
	void setAllAnswers(const std::string&, const std::string&, const std::string&, const std::string&);

	std::string getCorrectAnswer() const;
	void setStudentAnswer(const std::string&);

	int getId() const;
	void setId(const int);

	int getQuestionId() const;
	void setQuestionId(const int);

	bool operator==(const Answer&) const;
	friend std::ostream& operator<<(std::ostream&, const Answer&);


private:

	int m_id;
	int m_questionId;

	std::string m_answerA;
	std::string m_answerB;
	std::string m_answerC;
	std::string m_answerD;
	std::string m_correctAnswer;

};

