#pragma once

#include <QWidget>
#include <QTime>
#include <QTimer>
#include <QDateTime>
#include <QString>
#include <QMediaPlayer>
#include "ui_MainMenu.h"
#include "CourseFrame.h"
#include "Student.h"
#include "Questionnaire.h"
#include <QColorDialog>
#include <QColor>

class MainMenu : public QWidget
{
	Q_OBJECT

public:
	MainMenu(QWidget* parent = Q_NULLPTR);
	~MainMenu();
	void setStudent(Student&& student);
	void setGrades();

protected:
	void showEvent(QShowEvent* ev);

private slots:
	void on_FPButton_pressed();
	void on_BDButton_pressed();
	void on_SDButton_pressed();
	void on_POOButton_pressed();
	void on_Settings_pressed();
	void on_backButton_pressed();
	void on_sound_button_pressed();
	void on_changeBackgroundButton_pressed();
	void on_backColorButton_pressed();
	void on_changeColor_pressed();

	void updateTime();
	void updateMuteLabel();

private:
	Ui::MainMenu ui;
	CourseFrame m_bd_frame;
	CourseFrame m_sd_frame;
	CourseFrame m_poo_frame;
	CourseFrame m_fp_frame;

	void playSound();
	void changeColor();

private:
	Student m_student;
	bool m_isMuted = false;
	QString m_bgColor = "";
private:
	Questionnaire m_questionnaire;
};

