#include "pch.h"
#include "CppUnitTest.h"

#include "../QuestionnaireFramework/Questionnaire.h"
#include "../QuestionnaireFramework/Database.h"
#include "../QuestionnaireFramework/Questionnaire.cpp"
#include "../QuestionnaireFramework/Database.cpp"
#include "../QuestionnaireFramework/Answer.h"
#include "../QuestionnaireFramework/Answer.cpp"
#include "../QuestionnaireFramework/Question.h"
#include "../QuestionnaireFramework/Question.cpp"
#include "../QuestionnaireFramework/Networking.h"
#include "../QuestionnaireFramework/Networking.cpp"
#include "../QuestionnaireFramework/Email.h"
#include "../QuestionnaireFramework/Email.cpp"
#include "../Dependencies/CKMAIL/include/CkMailMan.h"
#include "../Dependencies/CKMAIL/include/CkEmail.h"

#include <string.h>
#include <regex>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuestionnaireTest
{
	TEST_CLASS(QuestionnaireTest)
	{
	public:

		TEST_METHOD(ConnectDatabase)
		{
			Database d;
			Assert::AreEqual(true, d.getIsConnected());
		}

		TEST_METHOD(VerifyQuestionTopic)
		{
			Database d;
			Questionnaire q;

			std::stringstream string;
			string << d.getIsConnected();

			q.setSubject("BD");
			q.chooseQuestions();

			int size = q.getQuestions().size();
			int noSize = 0;
			Assert::AreNotEqual(size, noSize);
		}

		TEST_METHOD(VerifyQuestionAnswers)
		{
			Database d;
			Questionnaire q("FP", d.getStmtHandle());

			std::stringstream string;
			string << d.getIsConnected();

			q.chooseQuestions();
			bool notNull = true;

			std::vector<Question> questions = q.getQuestions();
			for (Question question : questions)
			{
				std::vector<Answer> answers = question.getAllAnswers();
				for (Answer answer : answers)
				{
					for (int i = 1; i <= 4; i++)
					{
						std::string a = answer.getAnswer(i);
						if (a.length() == 0)
						{
							notNull = false;
						}
					}

				}
			}
			Assert::AreNotEqual(false, notNull);
		}

		TEST_METHOD(VerifyQuestionCorrectAnswer)
		{
			Database d;
			Questionnaire q("FP", d.getStmtHandle());

			std::stringstream string;
			string << d.getIsConnected();

			q.chooseQuestions();
			bool notNull = true;

			std::vector<Question> questions = q.getQuestions();
			for (Question question : questions)
			{
				for (int i = 1; i <= 4; i++) {
					std::string a = question.getCorrectAnswer().getAnswer(i);
					if (a.length() == 0)
					{
						notNull = false;
					}
				}
			}
			Assert::AreNotEqual(false, notNull);
		}

		TEST_METHOD(ConnectToNetwork_QuickMode)
		{
			Networking networking;
			Assert::AreEqual(true, networking.networkConnection_QuickMode());
		}

		TEST_METHOD(ConnectToNetwork_AdvancedMode)
		{
			Networking networking;
			Assert::AreEqual(true, networking.networkConnection_AdvancedMode());
		}

		TEST_METHOD(SendEmail)
		{
			Email email("Petre Bogdan", "petrebogdan55@yahoo.com", "Email Title", "Email Message");
			Assert::AreEqual(true, email.emailUnitTest());
		}
	};
};
