#include "LoggerSingleton.h"
Logger& LoggerSingleton::getLoggerInstance()
{
	static Logger log("log.txt");

	return log;
}