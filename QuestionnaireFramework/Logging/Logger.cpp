#include "Logger.h"

Logger::Logger(const std::string& filename) : m_fout(filename), m_os(m_fout), m_minimumLevel(Level::Info)
{

}

 void Logger::log(const std::string& message, Level level)
{
	
	switch (level)
	{
	case Level::Info:
		m_os << "[INFO]";
		break;
	case Level::Warning:
		m_os << "[WARNING]";
		break;
	case Level::Error:
		m_os << "[ERROR]";
		break;
	}
	std::time_t now = std::time(nullptr);
	m_os << '[' << std::put_time(localtime(&now), "%Y-%m-%d %H:%M:%S") << ']';
	m_os << " " << message;
	
}

void Logger::setMinimumLevel(Level minimumLevel)
{
	m_minimumLevel = minimumLevel;
}
