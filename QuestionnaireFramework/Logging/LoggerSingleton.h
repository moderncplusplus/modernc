#pragma once

#include "LoggingExports.h"
#include "Logger.h"

class LOGGING_API LoggerSingleton
{

public:
	LoggerSingleton(LoggerSingleton const&) = delete;
	LoggerSingleton(LoggerSingleton&&) = delete;
	LoggerSingleton& operator=(LoggerSingleton const&) = delete;
	LoggerSingleton& operator=(LoggerSingleton&&) = delete;

	static Logger& getLoggerInstance();

protected:
	LoggerSingleton() = default;
	~LoggerSingleton() = default;

};

